// ==================================
// ESLINT-SETTING - don't DEL comment's!!!
// ==================================
/*eslint no-undef: "error"*/
/*eslint-env node*/
// ==================================


// === Import params
'use strict';
import { task, src, dest, lastRun, series, parallel, watch } from 'gulp';
import { gifsicle, jpegtran, optipng } from 'gulp-imagemin';
import webpackStream from 'webpack-stream';
import resolver from 'stylus';
import log from 'fancy-log';
import path from 'path';
import del from 'del';
import fs from 'fs';


// === Node.js listener fix
// require('events').EventEmitter.prototype._maxListeners = 100;
import Events from 'events';
Events.defaultMaxListeners = 100;


// === Loader for gulp
const iniLoader = {
		rename: {
			'gulp-iconfont-css': 'iconfontCss',
			'gulp-babel-minify': 'babelMinify',
			'gulp-file-include': 'include',
			'gulp-css-url-adjuster': 'urlReplace',
			'gulp-string-replace': 'htmlReplace',
			'gulp-svg-sprite': 'svgSprite'
		}
	},
	_run = require('gulp-load-plugins')(iniLoader),
	isDevelopment = !process.env.NODE_ENV
	|| process.env.NODE_ENV === 'development',
	isPublic = process.env.NODE_ENV === 'public',


	// === Plugin list see in package.json
	browserSync = require('browser-sync').create(),
	combiner = require('stream-combiner2').obj,
	webpack = webpackStream.webpack;


// === Sources and directions for files
let
	inDev = 'development',
	inDevApps = 'development/components',
	inPub = 'public',
	inPubCss = inPub + '/css',
	inPubJs = inPub + '/js',
	dir = __dirname + '/';


// === REPLACE
const
	htmlSrcReplaceIn = 'src="./components/img/',
	htmlSrcReplaceOut = 'src="../media/img/',
	cssImageLink = '../media/img/',
	cssFontsLink = { replace: ['../media/img/fonts/', '../media/fonts/'] },
	iconfontReplace = { in: /(?:.DUMMYTEXT-|:before)/g, out: '$' },
	imageClearPath = { in: /^development\\components\\/gi, out: 'public\\media\\', },
	webpackAddCustomPlugins = {
		// picker: path.resolve('./node_modules/pickadate/lib/picker'),
	},
	webpackUglifyConfig = {
		cacheFolder: path.resolve(__dirname, `${inDevApps}/js/cached_uglify/`),
		debug: false,
		minimize: true,
		sourceMap: true,
		mangle: true,
		compress: {
			warnings: false, // Suppress uglification warnings
			pure_getters: true,
			unsafe: true,
			unsafe_comps: true,
			screw_ie8: true
		},
		output: {
			comments: false,
		},
		// plugin bundle ignore list
		exclude: [
			/\.min\.js$/gi,
			'global.js'
		]
	},
	errorConfig = (name, descript, err) => {
		return {
			title: `${name} - ${descript}`,
			message: 'Файл: \n\n' + err.message + '\n',
			sound: false
		};
	},
	includeConfig = {
		prefix: '<!-- @@',
		suffix: ' -->',
		basepath: '@file'
	},
	stylusLoadingConfig = {
		import: __dirname + `/${inDev}/tmp/sprite`,
		define: {
			url: resolver()
		}
	},
	imageClearCache = (filepath) => {
		let str = filepath;
		log('filepath origin', filepath);
		str = str.replace(imageClearPath.in, imageClearPath.out);
		log('filepath result', str);
		fs.unlink(str, (err) => {
			if (err) throw err;
			log('Deleted file: ', path.basename(filepath));
		});
	},
	spriteConfig = {
		mode: {
			css: {
				dest: '.',
				bust: !isDevelopment,
				sprite: 'sprite.svg',
				layouts: 'vertical',
				prefix: '$',
				dimensions: true,
				render: { styl: { dest: 'sprite.styl' } }
			}
		}
	},
	fontNameIni = 'font-glyphs',
	iconfontCssConfig = {
		fontName: fontNameIni,
		targetPath: dir + `${inDev}/tmp/glyphs.styl`,
		cssClass: 'DUMMYTEXT',
	},
	iconfontConfig = {
		fontName: fontNameIni,
		prependUnicode: true,
		formats: ['woff2'],
		normalize: true,
		fontHeight: 1001,
		timestamp: Math.round(Date.now() / 1000),
	},
	browserSyncConfig = {
		server: {
			baseDir: inPub,
			middleware: function(req, res, next) {
				res.setHeader('Access-Control-Allow-Origin', '*');
				next();
			}
		},
		ghostMode: false,
		notify: false,
		open: false,
		httpModule: 'spdy',
		port: 3000,
		https: true,
		'browser': 'chrome.exe'
	},
	webpackProvidePluginConfig = {
		'$': 'jquery',
		'jQuery': 'jquery',
		'window.jQuery': 'jquery',
		// 'owl.carousel': 'owl.carousel',
	},
	webpackINI = {
		stats: {
			colors: true,
			modules: true,
			reasons: true,
			errorDetails: true
		}
	},
	imageminConfig = [
		gifsicle({ interlaced: true }),
		jpegtran({ progressive: true }),
		optipng({ optimizationLevel: 5 })
	];


// === Install Plugins 
task('plugins:copy', () => {
	return combiner(
		src([
			// CSS
			dir + 'node_modules/normalize.css/normalize.css',
			dir + 'node_modules/lite-padding-margin/dist/css-source/lite-padding-margin.min.css',
			// dir + 'node_modules/owl.carousel/dist/assets/owl.carousel.min.css',
			// dir + 'node_modules/owl.carousel/dist/assets/owl.theme.default.min.css',
			// dir + 'node_modules/fullpage.js/dist/jquery.fullpage.min.css',
			// MAP
			// dir + 'node_modules/fullpage.js/dist/jquery.fullpage.min.css.map'
		], { since: lastRun('plugins:copy') }),
		_run.newer(`${inDevApps}/plugins`),
		dest(`${inDevApps}/plugins`)
	);
});


// === Move files 
// * HTML
task('html:move', () => {
	return combiner(
		src(`${inDev}/*.html`, { since: lastRun('html:move') }),
		_run.cached('html:move'),
		_run.htmlReplace(htmlSrcReplaceIn, htmlSrcReplaceOut),
		_run.include(includeConfig),
		_run.remember('html:move'),
		_run.newer(inPub),
		dest(inPub)).on('error',
		_run.notify.onError((err) => errorConfig('HTML', 'ошибка include', err)));
});
// * HTML add html parts path
task('html:move:layouts', () => {
	return combiner(
		src(`${inDev}/*.html`),
		_run.include(includeConfig),
		dest(inPub)).on('error',
		_run.notify.onError((err) => errorConfig('layouts', 'ошибка include', err)));
});
// * HTML replace html urls
task('html:move:urlreplace', () => {
	return combiner(
			src(`${inDev}/*.html`, { since: lastRun('html:move:urlreplace') }),
			_run.htmlReplace(htmlSrcReplaceIn, htmlSrcReplaceOut),
			dest(inDev))
		&& combiner(
			src(`${inDev}/layouts/**/*.html`),
			_run.htmlReplace(htmlSrcReplaceIn, htmlSrcReplaceOut),
			dest(`${inDev}/layouts`)
		).on('error',
			_run.notify.onError((err) => errorConfig('Urlreplace', 'ошибка include', err)));
});


// === Build NODE content
// * Fonts
task('fonts:loading', () => {
	return combiner(
		src(`${inDevApps}/fonts/**/*.{ttf,eot,otf,woff,woff2}`, ''),
		_run.newer(`${inPub}/media/fonts`),
		dest(`${inPub}/media/fonts`));
});
// * SEO-stuff
task('seo:stuff', () => {
	return combiner(
		src([
			`${inDevApps}/favicon.png`,
			`${inDevApps}/favicon.ico`,
			`${inDevApps}/robots.txt`,
			`${inDevApps}/.htaccess`,
		], { since: lastRun('seo:stuff') }),
		_run.newer(`${inPub}/`),
		dest(`${inPub}/`)).on('error',
		_run.notify.onError((err) => errorConfig('SEO-stuff', 'файл не найден', err)));
});
// * CSS
task('plugins:move:css', () => {
	return combiner(
		src(`${inDevApps}/plugins/**/*.css`),
		_run.newer(inPubCss),
		_run.concat('plugins.css'),
		dest(inPubCss));
});
// * MAP
task('plugins:move:map', () => {
	return combiner(
		src(`${inDevApps}/plugins/**/*.map`),
		_run.newer(inPubCss),
		dest(inPubCss));
});
// * JS
task('plugins:move:js', () => {
	return combiner(
		src(`${inDevApps}/plugins/**/*.js`),
		_run.newer(inPubJs),
		_run.concat('plugins.js'),
		dest(inPubJs));
});


// === Stylus params 
task('stylus:loading', () => {
	return combiner(
		src(`${inDevApps}/stylus/**/global.styl`),
		_run.if(isDevelopment, _run.sourcemaps.init()),
		_run.stylus(stylusLoadingConfig),
		_run.autoprefixer({ browsers: ['last 5 version'] }),
		_run.if(isDevelopment, _run.sourcemaps.write()),
		_run.urlReplace({ prepend: cssImageLink }),
		_run.urlReplace(cssFontsLink),
		dest(inPubCss)).on('error',
		_run.notify.onError((err) => errorConfig('Stylus', 'ошибка синтаксиса', err)));
});


// === Webpack params 
task('webpack', (flag) => {
	const path = require('path');
	const PATHS = {
		root: path.join(__dirname, ''),
		app: path.join(__dirname, `${inDevApps}/js/`),
	};

	let
		firstRun = true,
		init = {
			entry: { global: path.join(__dirname, `${inDevApps}/js/global`) },
			output: {
				path: PATHS.root,
				filename: '[name].js',
				sourceMapFilename: '[name].map'
			},
			stats: webpackINI.stats,
			watch: isDevelopment,
			devtool: isDevelopment ? 'cheap-module-inline-source-map' : false,
			module: {
				rules: [{
					test: /\.js$/,
					exclude: /node_modules/,
					use: {
						loader: 'babel-loader',
						options: { presets: ['@babel/preset-env'] }
					}
				}]
			},
			resolve: {
				modules: ['node_modules'],
				extensions: ['.js'],
				alias: webpackAddCustomPlugins
			},
			plugins: [
				new webpack.DefinePlugin(isDevelopment),
				new webpack.optimize.OccurrenceOrderPlugin(),
				new webpack.NoEmitOnErrorsPlugin(),
				new webpack.ProvidePlugin(webpackProvidePluginConfig),
				new webpack.optimize.UglifyJsPlugin(webpackUglifyConfig),
				new webpack.optimize.CommonsChunkPlugin({
					name: 'plugins',
					minChunks: module => module.context && module.context.indexOf('node_modules') !== -1
				})
			],
		};
	return combiner(
		src(`${inDevApps}/js/**/*.js`), webpackStream(init),
		dest(inPubJs).on('data', () => firstRun ? flag() : null || ''));
});


// === All media GRATHIC - jpg svg png 
// * jpg,jpeg,png,gif
task('img:all', () => {
	return combiner(
		src(`${inDevApps}/img/**/*.{jpg,jpeg,png,gif}`),
		_run.if(isPublic, _run.filesize()),
		dest(`${inPub}/media/img`)
	);
});
// * svg
task('img:svg', () => {
	return combiner(
		src(`${inDevApps}/img/__sprite/**/*.svg`, { allowEmpty: true }),
		_run.svgSprite(spriteConfig),
		_run.if('*.styl',
			dest(`${inDev}/tmp`),
			dest(`${inPub}/media/img`))
	);
});
// * svg font generator
task('img:tosvg', () => {
	return combiner(
		src(`${inDevApps}/img/__glyphs/**/*.svg`, { allowEmpty: true }),
		_run.iconfontCss(iconfontCssConfig),
		_run.replace(iconfontReplace.in, iconfontReplace.out),
		_run.replace('@font-face', '.DUMMYTEXT__'),
		_run.iconfont(iconfontConfig),
		dest(`${inPub}/media/fonts/font-glyphs/`)
	);
});


// === Compress IMG 
task('img:compress', () => {
	return combiner(
		src(`${inDevApps}/img/**/*.{jpg,jpeg,png,gif}`),
		_run.newer(`${inPub}/media/img/`),
		_run.cached('imgcache'),
		_run.if(isPublic, _run.imagemin(imageminConfig)),
		_run.if(isPublic, _run.filesize()),
		_run.remember('imgcache'),
		dest(`${inPub}/media/img`)
	);
});


// === Minification bundls 
// * CSS
task('mini:css', () => {
	return combiner(
		src(`${inPub}/css/global.css`, { allowEmpty: true }),
		_run.if(isPublic, _run.cssnano()),
		_run.rename({ suffix: '.min' }),
		dest(inPubCss));
});
// * JS
task('mini:js', () => {
	return combiner(
		src(`${inPub}/js/global.js`, { allowEmpty: true }),
		_run.if(isPublic, _run.babelMinify()),
		_run.rename({ suffix: '.min' }),
		dest(inPubJs));
});


// === Clean Public folder 
task('deleteFiles', () =>
	del([inPub, dir + `${inDevApps}/plugins/`], { read: false })
);


// === Server params 
task('server:loading', () => {
	browserSync.init(browserSyncConfig), browserSync.watch(`${inPub}/**/*.*`)
		.on('change', browserSync.reload);
});


// === Watch all files 
task('watchme', () => {
	// * watch Dev
	watch(`${inDev}/*.html`, series('html:move'));
	watch(`${inDevApps}/img/**/*.{jpg,jpeg,png,gif}`, series('img:all'))
		.on('unlink', (filepath) => imageClearCache(filepath));
	watch(`${inDevApps}/fonts/**/*.{ttf,eot,otf,woff,woff2}`, series('fonts:loading'));
	watch(`${inDev}/layouts/**/*.html`, series('html:move:layouts'));
	watch(`${inDevApps}/plugins/**/*.{css}`, parallel('plugins:move:css'));
	watch([`${inDevApps}/stylus/**/*.{styl,stylus}`, `${inDev}/tmp/sprite.styl`], series('stylus:loading'));
	watch(`${inDevApps}/img/__sprite/**/*.svg`, series('img:svg'));
	watch(`${inDevApps}/img/__glyphs/**/*.svg`, series('img:tosvg'));
	// * watch Pub
	watch(`${inPub}/css/**/*.css`);
});


// === Gulp setup loading tasks 
task('loadingGulpConfig', series(
	parallel('img:all', 'img:svg', 'img:tosvg', 'seo:stuff', 'fonts:loading'),
	parallel('html:move', 'html:move:layouts'),
	parallel('html:move:urlreplace'),
	parallel('plugins:copy', 'stylus:loading'),
	parallel('plugins:move:css', 'plugins:move:map'),
	parallel('webpack'),
	parallel('mini:css', 'mini:js'),
));


// === Start gulp config - yarn dev 
task('default',
	series('deleteFiles', 'loadingGulpConfig', parallel('server:loading', 'watchme')));